import React, {Component} from 'react';

class Button extends Component {
    render() {
        return (
            <button onClick={this.props.handleClickParent}>{this.props.isOpen ? "Свернуть" : "Развернуть"}</button>
        )
    }
}

export default Button;
