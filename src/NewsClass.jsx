import React, {Component} from 'react';

class Post extends Component {
    constructor(props) {
        super(props);
        this.op = props.post.op
        this.state = {
            isOpen: this.op
        }
        

        this.handleClick = this.handleClick.bind(this);
    }

    shouldComponentUpdate(nextProps) {
        this.state.isOpen = nextProps.post.op
        return this.state
    }

    render() {
        let title = (
            <div>
            <h2>{this.props.post.img}</h2>
            <h2>{this.props.post.title}</h2>
            </div>
        )
        let info;
        if(this.state.isOpen) {
            info = (
                <div>
                    <h2>{this.props.post.content}</h2>
                </div>
            );
        }
        

        return (
            <div>
                {title}
                <button onClick={this.handleClick}>{(this.state.isOpen)? "Свернуть" : "Развернуть"}</button>
                {this.state.isOpen && info}
            </div>
        )
    }

    handleClick() {
        
        this.setState({
            isOpen: !this.state.isOpen
        })
    }
}

export default Post;
