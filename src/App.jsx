import React, { Component } from 'react';
import Post from "./NewsClass";
import { news } from './News';
class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            posts: news,
            value: ''
        }

    }

    render() {
        if (this.state.posts.length > 0) {
            this.state.posts[0].op = true
            this.state.posts[this.state.posts.length - 1].op = false
        }
        let postsList = this.state.posts.map(post => <Post key={post.id} post={post} />)
        return (
            <>
                <div>Поиск новости</div>
                <input onChange={this.handleChange} value={this.state.value}></input>
                <button onClick={this.findPost}>Найти новость</button>
                <button onClick={this.revertPosts}>Revert</button>
                <button onClick={this.resetPosts}>Сброс</button>
                <h1>Последние новости</h1>
                {postsList}
            </>
        )
    }

    handleChange = (event) => {
        this.setState({
            value: event.target.value
        })
    }


    findPost = () => {
        this.setState({
            posts: this.state.posts.filter((post) => post.title.toLocaleLowerCase() === this.state.value.toLocaleLowerCase()),
            value: ''
        })
    }

    revertPosts = () => {
        this.setState({
            posts: this.state.posts.reverse()
        })

    }
    resetPosts = () => {
        this.setState({
            posts: news
        })

    }

}

export default App;

